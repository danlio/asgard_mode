let main = new function () {
	let $this = this;
	const dl = require('download-file-with-progressbar');
	const request = require('request');
	const cheerio = require('cheerio');
	const fs = require('fs');
	const gui = require('nw.gui');
	let modeList = [];
	let option = {
		'url': 'http://www.asgard.idv.tw/mods/',
		'userPath': '/AppData/Roaming/.minecraft/mods',
		//'userPath': '/tmps/asgard',
		'minecraftPath': 'C:/Program Files (x86)/Minecraft/MinecraftLauncher.exe'
	};

	createList = function () {
		$('.cls_modeList').html('');
		for (let i in modeList) {
			let msg = $('<div/>')
				.css({ 'padding': '2px' })
				.append(
					$('<i/>')
						.text("Update Time : " + modeList[i].updateTime)
						.css({ 'float': 'left' })
				)
				.append(
					$('<i/>')
						.text("Size : " + modeList[i].fileSize)
						.css({ 'float': 'right', 'padding-right': '5px' }
						)
				);
			let progress = $('<div/>')
				.addClass('progress')
				.append(
					$('<div/>')
						.addClass('progress-bar bg-danger')
						.attr({
							role: 'progressbar',
							'aria-valuenow': '0',
							'aria-valuemax': '100',
							id: 'mode_' + i
						})
						.css({ width: '0%' })
				);
			let strong = $('<strong/>')
				.addClass('d-block text-dark')
				.text(modeList[i].name)
				.append(
					$("<button/>")
						.attr({ 'type': 'button', data: i })
						.addClass('badge badge-pill badge-warning cls_mods_btn cls_download_hidden')
						.css({'margin-left': '5px', 'margin-bottom': '5px'})
						.text('download')
						.click(function () {
							let id = $(this).attr('data');
							let homedir = require('os').homedir();
							let opt = {
								filename: modeList[id].name,
								dir: homedir + option.userPath,
								onDone: (info) => {
									let num = $('.cls_now_mode').text();
									let total = $('.cls_total_mode').text();
									if ((num * 1) < (total * 1) ) {
										$(".cls_now_mode").text((num * 1) + 1);
									}
									check_end();
								},
								onError: (err) => {
									$(this).removeClass('cls_download_hidden');
									$(".alert").removeClass('cls_hidden').text(err);
								},
								onProgress: (curr, total) => {
									let num = (curr / total * 100).toFixed(2);
									$('#mode_' + id).width(num + '%').text(num + '%');
								},
							}
							dl(option.url + modeList[id].name, opt);
						}
						)
				);
			$('.cls_modeList')
				.append($('<div/>')
					.addClass('media text-muted pt-3')
					.append(
						$('<p/>')
							.addClass('media-body pb-3 mb-0 small lh-125 border-bottom border-gray')
							.append(strong)
							.append(progress)
							.append(msg)
					)
				);
		}
		event_btn();
	}

	getModes = function () {
		request(
			{ url: option.url },
			function (error, response, body) {
				if (response.statusCode == 200) {
					let $c = cheerio.load(body);
					$c('pre a').each(function (i, elem) {
						if (i != 0) {
							let tmp = {};
							tmp.name = elem.children[0].data;
							let tmpStr = elem.prev.data.split(' ').filter(n => n);
							tmp.updateTime = tmpStr[0] + " " + tmpStr[1] + " " + tmpStr[2];
							tmp.fileSize = tmpStr[3];
							modeList.push(tmp);
						}
					});
					createList();
					$(".cls_total_mode").text(modeList.length);
				} else {
					$(".alert")
						.removeClass('cls_hidden')
						.text("GET Mode Error : " + response.statusCode);
				}
			}
		);
	}

	event_btn = function () {
		$('.cls_btn_right').click(function () {
			$('.cls_mods_btn').each(function () {
				$(this).trigger('click');
			});
			$(this).css({ 'display': 'none' });
		});
		$('.cls_start').click(function(){
			open_minecraft();
		});
	}

	check_end = function () {
		if ($(".cls_now_mode").text() == modeList.length) {
			$(".alert").removeClass('cls_hidden');
			if (require('os').platform == 'win32') {
				open_minecraft();
			}
		}
	}
  
	open_minecraft = function () {
		if (fs.existsSync(option.minecraftPath)) {
			let exec = require('child_process').execFile;
			exec(option.minecraftPath, function (err, data) {
				gui.App.quit();
			});
		} else {
			$(".modal-body div p ").html('遊戲啟動路徑錯誤 ! <br/>啟動遊戲預設路徑 : <br/>' + option.minecraftPath);
			$(".cls_error").modal('show');
		}
	}

	check_dir = function () {
		let dir = require('os').homedir() + option.userPath;
		if (!fs.existsSync(dir)) {
			try {
				fs.mkdirSync(dir);
			} catch (error) {
				console.log(error);
			}
		}
	}

	init = function () {
		getModes();
		if (require('os').platform == 'win32') {
			check_dir();
		}
	}
	init();
}